import subprocess
import re
import matplotlib.pyplot as plt
import numpy as np
import time

N = 512
fs = 44100
sample_rate = 1 / fs
sample_time = sample_rate * N
t = np.arange(0.0, sample_time, sample_rate)
v_in =  1 * np.sin(2 * 344.531 * np.pi * t) 
# v_in += 0.9 * np.sin(2 * 2497.851 * np.pi * t)
v_in += 0.8 * np.sin(2 * 4995.703 * np.pi * t) 
# v_in += 0.7 * np.sin((2 * 7493.554 * np.pi * t))# + np.pi/2)
v_in += 0.6 * np.sin(2 * 9991.406 * np.pi * t) 
# v_in += 0.5 * np.sin(2 * 12489.257 * np.pi * t)
# v_in += 0.4 * np.sin(2 * 14987.109 * np.pi * t)
# v_in += 0.3 * np.sin(2 * 19982.812 * np.pi * t)
v_noise = v_in # + (1.2 * np.random.normal(0, 1, len(t)))

fp = open("v_in.csv", "w")
for val in v_noise:
    fp.write(f"{val:.5f}\n")
fp.close()


def read_fft():
    returned_output = subprocess.check_output("./fft_c")
    fft_c = returned_output.decode("utf-8")
    fft_values = fft_c.split("\n")

    tick = []
    real = []
    f = []
    amp = []
    phase = []

    for line in fft_values:
        # print(f"L: {line}")
        m = re.match("([0-9]*).*,\((.*),(.*)\),\((.*),(.*)\)", line)
        if m:
            # print(f"M: {m.group(0)}")
            if m.group(5):
                n = float(m.group(1))
                tick.append(n)
                real.append(float(m.group(2)))
                if n < N/2:
                    f.append(n/sample_time)
                    amp.append(float(m.group(4)))
                    phase.append(float(m.group(5)))
                
    # print(f"T {tick}")
    # print(f"R {real}")
    # print(f"A {amp}")
    # print(f"P {phase}")

    return tick, real, f, amp, phase


tick, real, f, amp, phase = read_fft()
fig, ax = plt.subplots(4, 1)
fig.set_size_inches(18.5, 10.5)
ax[0].plot(t, v_in, marker=".")
ax[1].plot(t, real, marker=".")
ax[2].stem(f, amp)

print(f"fs: {1/sample_rate}")
print(f"N : {len(v_in)}")
print(f"T : {sample_time}")
fft_np = np.fft.fft(v_in)
fft_np = fft_np[:int(N / 2)]
ax[3].stem(f, np.absolute(fft_np))
plt.show()
