#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <time.h>

#include "fft_cooley.h"
#include "kiss_fft.h"

void print_data(float *left, float *right, int len){
    int idx = 0;
    for(int k = 0; k < len; k++){
            printf("%-5d,(%-8.3f,%-8.3f),(%-8.3f,%-8.3f)\n", idx++, left[k], left[k+1], right[k], right[k+1]);
            k++;
        }
}

void amp_theta(float *data, int len){
    for(int k = 0; k < (len << 1); k++){
        float real = data[k];
        float imaginary = data[k + 1];
        data[k] = sqrt((real*real) + (imaginary*imaginary));
        data[k + 1] = 0;
        if(data[k])
           data[k + 1] = atan(imaginary/real);
           
        k++;
    }
}

#define FFT_MAX 1024
int fill_input(float *data, int len){
    float step = 1.0 / (float)(len); 
    float t = 0;
    for(int k = 0; k < len; k++){
        float theta = M_PI * 2 * t * 5;
        t += step;
        data[k] = sin(theta) + (0.5 * sin(theta/5));
    }

    return len;
}

int read_input(const char *filename, float *data, int len){

    FILE *fp = fopen(filename, "r");
    if(!fp)
    {
        printf("Could NOT open %s\n", filename);
    }

    printf("Opened: %s\n", filename);
    size_t line_len = 0;
    char *line = 0;
    int read_len = 0;    
    int idx = 0;
    do {
        read_len = getline(&line, &line_len, fp);
        float val = atof(line);
        // printf("R[%d]: %f\n", idx, val);
        data[idx] = val;
        if(idx++ > len)
            break;
    }while(read_len > 0);

    free(line);

    fclose(fp);

    return idx - 1;
}

void do_kiss(float *values, int len)
{
    kiss_fft_cfg cfg;
    kiss_fft_cpx *yt = malloc(len * sizeof(kiss_fft_cpx));
    kiss_fft_cpx *yF = malloc(len * sizeof(kiss_fft_cpx));

    //every second float value is the real part
    int idx = 0;
    for (int i = 0; i < len; i++)
    {
        yt[i].r = values[idx];
        idx += 2;
        // yt[i].r = (values[i] + 0.000062), yt[i].i = 0; //adding negligible amount to ensure no division by zero
    }

    if ((cfg = kiss_fft_alloc(len, 0/*is_inverse_fft*/, NULL, NULL)) != NULL)
    {
        kiss_fft(cfg, yt, yF);
        free(cfg);
    }

    idx = 0;
    for(int k = 0; k < (len << 1); k++)
    {
        values[k++] = yF[idx].r;
        values[k] = yF[idx].i;
        idx++;
    }

    free(yt);
    free(yF);
}

int main()
{
    float in[FFT_MAX];

    float v_in[FFT_MAX];
    //int len = fill_input(v_in, FFT_N);
    int len = read_input("v_in.csv", v_in, FFT_MAX / 2);
    printf("FFT in C\nN: %d\n", len);

    int idx = 0;
    for(int k = 0; k < len; k++){
        in[idx++] = v_in[k];
        in[idx++] = 0;
    }
    

    float out[FFT_MAX];
    memcpy(out, in, sizeof(in));

    clock_t begin = clock();
    fft_cooley_tukey(out, len);
    // do_kiss(out, len);
    clock_t end = clock();
    float time_spent = (float)(end - begin) / CLOCKS_PER_SEC;
    printf("Time spent %f\n", time_spent);
    
    amp_theta(out, len);
    print_data(in, out, (len << 1));

    return 0;
}