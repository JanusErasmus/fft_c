# Implement FFT - Cooley and Tukey FFT [1965]

Python was used to plot and test the transform

 - [matplotlib](https://matplotlib.org/) was used to plot results of the filter
 - Build `main.c` using  
`gcc main.c -lm  -o fft_c`
 - Run `python3 main.py` to plot a response