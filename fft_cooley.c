#include <stdint.h>
#include <math.h>

static void swap(float *a, float *b)
{
    float tmp = *a;
    *a = *b;
    *b = tmp;
}
// Cooley and Tukey FFT [1965]
//  The initial signal is stored in the array data of length 2*nn,
//  where each even element corresponds to the real part
//  and each odd element to the imaginary part of a complex number.
void fft_cooley_tukey(float* data, unsigned long nn)
{
    unsigned long n, mmax, m, j, istep, i;
    float wtemp, wr, wpr, wpi, wi, theta;
    float tempr, tempi;

    // //scale input signal
    // for(int k = 0; k < (nn << 1); k++){
    //     data[k] = data[k] / nn;  
    //     //printf("%-6.3f,%-6.3f\n", theta, in[k]);
    //     k++;
    // }
 
    // reverse-binary reindexing
    n = nn<<1;
    j=1;
    for (i=1; i<n; i+=2) {
        if (j>i) {
            swap(&data[j-1], &data[i-1]);
            swap(&data[j], &data[i]);
        }
        m = nn;
        while (m>=2 && j>m) {
            j -= m;
            m >>= 1;
        }
        j += m;
    };
 
    // here begins the Danielson-Lanczos section
    mmax=2;
    while (n>mmax) {
        istep = mmax << 1;
        theta = -(2*M_PI/mmax);
        wtemp = sin(0.5*theta);
        wpr = -2.0*wtemp*wtemp;
        wpi = sin(theta);
        wr = 1.0;
        wi = 0.0;
        for (m=1; m < mmax; m += 2) {
            for (i=m; i <= n; i += istep) {
                j=i+mmax;
                tempr = wr*data[j-1] - wi*data[j];
                tempi = wr * data[j] + wi*data[j-1];
 
                data[j-1] = data[i-1] - tempr;
                data[j] = data[i] - tempi;
                data[i-1] += tempr;
                data[i] += tempi;
            }
            wtemp = wr;
            wr += wr*wpr - wi*wpi;
            wi += wi*wpr + wtemp*wpi;
        }
        mmax=istep;
    }

    // //re-scale input signal
    // for(int k = 0; k < (nn << 1); k++){
    //     data[k] = data[k] * nn;
    //     k++;
    //     data[k] = data[k] * nn;
    // }
}