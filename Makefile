TARGET = fft_c
BUILD_DIR = build


C_SOURCES =  \
main.c  \
fft_cooley.c \
kiss_fft.c

# C_INCLUDES =

CC = $(PREFIX)gcc

OBJECTS = $(addprefix $(BUILD_DIR)/,$(notdir $(C_SOURCES:.c=.o)))

CFLAGS = $(C_DEFS) $(C_INCLUDES)

LIBS = -lm
LDFLAGS = $(LIBS)

all: $(BUILD_DIR)/$(TARGET)


$(BUILD_DIR)/%.o: %.c | $(BUILD_DIR) 
	@echo 'GCC $<' 
	@$(CC) -c $(CFLAGS) $< -o $@

$(BUILD_DIR)/$(TARGET): $(OBJECTS)
	@echo 'LDD $@'
	@$(CC) $(OBJECTS) $(LDFLAGS) -o $@

$(BUILD_DIR):
	mkdir $@	